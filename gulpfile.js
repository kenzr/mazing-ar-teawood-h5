var gulp=require("gulp");
var spritesmith=require('gulp.spritesmith');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant'); //png图片压缩插件
//合成雪碧图
gulp.task('sprite',function(){
  return gulp.src('src/assets/adWall/*.png')
    .pipe(spritesmith({
      imgName:'adWallSprite.png',//保存合并后图片的地址
      cssName: 'css/spritet.css',//保存合并后对于css样式的地址
      algorithm:'left-right',
      //algorithm:'top-down',
      padding:1,//合并时两个图片的间距
    }))
    .pipe(gulp.dest('dist/adWall'));
})
//压缩图片
gulp.task('imgmin', function () {
  return gulp.src('src/assets/sayhi/*.png')
    .pipe(imagemin({
      progressive: true,
      use: [pngquant()] //使用pngquant来压缩png图片
    }))
    .pipe(gulp.dest('dist'));
});
