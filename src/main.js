import Vue from 'vue'
import vueResource from 'vue-resource'
import App from './App'

import './less/common.less'

Vue.use(vueResource)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
